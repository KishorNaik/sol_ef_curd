﻿using Sol_EF_Curd.Dal;
using Sol_EF_Curd.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Curd
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                UserDal userDalObj = new UserDal();
                //await userDalObj.AddAsync(new UserEntity() {
                //    FirstName="Kishor",
                //    LastName="Naik"
                //});

                //await userDalObj.UpdateAsync(new UserEntity()
                //{
                //    UserId=1,
                //    FirstName = "Eshaan",
                //    LastName = "Naik"
                //});

                await userDalObj.DeleteAsync(new UserEntity()
                {
                    UserId = 1
                });

            }).Wait();
        }
    }
}
