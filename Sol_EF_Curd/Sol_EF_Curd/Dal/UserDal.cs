﻿using Sol_EF_Curd.EF;
using Sol_EF_Curd.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity; // for Async Linq Extension

namespace Sol_EF_Curd.Dal
{
    public class UserDal
    {
        #region Declaration
        private UserDBEntities db = null;
        #endregion

        #region Construtor
        public UserDal()
        {
            db = new UserDBEntities();
        }
        #endregion

        #region Public Methods
        public async Task<Boolean> AddAsync(UserEntity userEntityObj)
        {
            try
            {
                db.tblUsers.Add(await this.UserMappingAsync(new tblUser(), userEntityObj));

                await db.SaveChangesAsync();

                return true;
            }
            catch(Exception)
            {
                throw;
            }
        }

        public async Task<Boolean> UpdateAsync(UserEntity userEntityObj)
        {
            try
            {
                // get tblUser data based on ID
                tblUser tblUserObj = await this.GetByIdAsync(userEntityObj);

                // mapping User Data
                await this.UserMappingAsync(tblUserObj, userEntityObj);

                await db.SaveChangesAsync();

                return true;
            }
            catch(Exception)
            {
                throw;
            }
        }

        public async Task<Boolean> DeleteAsync(UserEntity userEntityObj)
        {
            try
            {
                // get tblUser data based on ID
                tblUser tblUserObj = await this.GetByIdAsync(userEntityObj);

                // remove data from tblUser
                db.tblUsers.Remove(tblUserObj);

                await db.SaveChangesAsync();

                return true;
            }
            catch(Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Method
        private async Task<tblUser> UserMappingAsync(tblUser tblUserObj,UserEntity userEntity)
        {
            try
            {
                return await Task.Run(() => {

                    
                    tblUserObj.FirstName = userEntity.FirstName;
                    tblUserObj.LastName = userEntity.LastName;
                    tblUserObj.UserId = userEntity.UserId;


                    return tblUserObj;
                });
            }
            catch(Exception)
            {
                throw;
            }
        }

        private async Task<tblUser> GetByIdAsync(UserEntity userEntityObj)
        {
            try
            {

                return await 
                db
                    ?.tblUsers
                    ?.AsQueryable()
                    ?.Where(
                        (letblUserObj) => letblUserObj.UserId == userEntityObj.UserId
                        )
                    ?.FirstOrDefaultAsync();
            }
            catch(Exception)
            {
                throw;
            }
        }
        #endregion 
    }
}
